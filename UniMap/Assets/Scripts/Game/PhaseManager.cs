using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PhaseManager : MonoBehaviour
{
    [SerializeField] private Transform[] m_DominationPointsLocation;
    [SerializeField] private Transform m_FinalDominationPointLocation;

    [Header ("Text")]
    [SerializeField] private TextMeshProUGUI m_LocationText;
    [SerializeField] private TextMeshProUGUI m_BoostText;
    [SerializeField] private GameObject m_YouWinUI;

    [Header ("Prefabs")]
    [SerializeField] private GameObject m_DominationPoint;
    [SerializeField] private GameObject m_Enemy;
    [SerializeField] private GameObject m_BigEnemy;
    [SerializeField] private GameObject m_RangedEnemy;
    [SerializeField] private GameObject m_Boss;
    [SerializeField] private GameObject m_RangedBoss;
    
    private Transform m_Player;
    private int m_Phase = 1;
    private bool m_PhaseCompleted = true;
    private List<int> m_LocationPass = new List<int>();
    private List<int> m_Boosts = new List<int>();
    private int m_TotalBoosts = 8;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        m_Phase = 1;
        m_Player = GameObject.Find("Player").transform;
    }

    private void Update()
    {
        if (m_PhaseCompleted)
        {
            if (m_Phase > 1)
            {
                GetRandomBoost();
            }

            m_PhaseCompleted = false;
            int index = Random.Range(0, m_DominationPointsLocation.Length);
            while (m_LocationPass.Contains(index) && !m_LocationPass.Count.Equals(0))
                index = Random.Range(0, m_DominationPointsLocation.Length);
            m_LocationPass.Add(index);
            UpdateLocationText(index);

            if (m_Phase == 1)
                StartCoroutine(Phase1(20, m_DominationPointsLocation[index]));
            else if (m_Phase == 2)
                StartCoroutine(Phase2(20, m_DominationPointsLocation[index]));
            else if (m_Phase == 3)
                StartCoroutine(Phase3(20, m_DominationPointsLocation[index]));
            else if (m_Phase == 4)
                StartCoroutine(Phase4(20, m_DominationPointsLocation[index]));
            else if (m_Phase == 5)
                StartCoroutine(Phase5(20));
            else if (m_Phase == 6)
                Win();
        }
    }
    
    private void GetRandomBoost()
    {
        int index = Random.Range(0, m_TotalBoosts);
        while (m_Boosts.Contains(index) && !m_Boosts.Count.Equals(0))
            index = Random.Range(0, m_TotalBoosts);
        m_Boosts.Add(index);
        
        string boostText = "nothing";
        if (index == 0) 
        {
            boostText = "Double health";
            PlayerHealth playerHealth = m_Player.GetComponent<PlayerHealth>();
            playerHealth.m_MaxHealth *= 2;
            playerHealth.Heal(playerHealth.m_MaxHealth);
        }
        else if (index == 1)
        {
            boostText = "Speed up";
            PlayerMotor playerMotor = m_Player.GetComponent<PlayerMotor>();
            playerMotor.m_Speed += 2;
        }
        else if (index == 2)
        {
            boostText = "Double damage";
            Gun gun = m_Player.GetComponent<Gun>();
            gun.m_Damage *= 2;
        }
        else if (index == 3)
        {
            boostText = "More ammo";
            Gun gun = m_Player.GetComponent<Gun>();
            gun.m_TotalAmmo += 10;
            gun.UpdateAmmo();
        }
        else if (index == 4)
        {
            boostText = "Reloading faster";
            Gun gun = m_Player.GetComponent<Gun>();
            gun.m_ReloadingTime /= 2;
        }
        else if (index == 5)
        {
            boostText = "Increase fire rate";
            Gun gun = m_Player.GetComponent<Gun>();
            gun.m_FireCooldown /= 2;
        }
        else if (index == 6)
        {
            boostText = "Jump higher";
            PlayerMotor playerMotor = m_Player.GetComponent<PlayerMotor>();
            playerMotor.m_JumpHeight += 0.5f;
        }
        else if (index == 7)
        {
            boostText = "Explosion Bullet";
            Gun gun = m_Player.GetComponent<Gun>();
            gun.m_IsExplosionBullet = true;
        }

        m_BoostText.text = boostText;
        StartCoroutine(DisplayBoost());
    }

    private IEnumerator DisplayBoost()
    {
        yield return new WaitForSeconds(3);
        m_BoostText.text = "";
    }

    private void UpdateLocationText(int index)
    {
        string locationName = m_DominationPointsLocation[index].GetComponent<LocationName>().LocationText;

        if (m_Phase == 5)
            locationName = m_FinalDominationPointLocation.GetComponent<LocationName>().LocationText;
        m_LocationText.text = "Stay at " + locationName + " for 20 seconds";
    }

    private IEnumerator Phase1(int delayTime, Transform dominationPointLocation)
    {
        var dominationPoint = Instantiate(m_DominationPoint, 
                                          dominationPointLocation.position, 
                                          dominationPointLocation.rotation);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_Enemy, spawnPoint.position, spawnPoint.rotation);  
        }

        while (m_Phase == 1)
        {    
            int time = dominationPoint.GetComponent<DominationPoint>().GetSeconds();
            
            if (time >= 20)
            {
                Destroy(dominationPoint);
                m_Phase = 2;
                m_PhaseCompleted = true;
                break;
            }

            yield return new WaitForSeconds(delayTime);
        }
    }

    private IEnumerator Phase2(int delayTime, Transform dominationPointLocation)
    {
        var dominationPoint = Instantiate(m_DominationPoint, 
                                          dominationPointLocation.position, 
                                          dominationPointLocation.rotation);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_Enemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_BigEnemy, spawnPoint.position, spawnPoint.rotation);  
        }

        while (m_Phase == 2)
        {    
            int time = dominationPoint.GetComponent<DominationPoint>().GetSeconds();
            
            if (time >= 20)
            {
                Destroy(dominationPoint);
                m_Phase = 3;
                m_PhaseCompleted = true;
                break;
            }

           yield return new WaitForSeconds(delayTime);
        }
    }

    private IEnumerator Phase3(int delayTime, Transform dominationPointLocation)
    {
        var dominationPoint = Instantiate(m_DominationPoint, 
                                          dominationPointLocation.position, 
                                          dominationPointLocation.rotation);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_Enemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_BigEnemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_RangedEnemy, spawnPoint.position, spawnPoint.rotation);  
        }

        while (m_Phase == 3)
        {    
            int time = dominationPoint.GetComponent<DominationPoint>().GetSeconds();
            
            if (time >= 20)
            {
                Destroy(dominationPoint);
                m_Phase = 4;
                m_PhaseCompleted = true;
                break;
            }

            yield return new WaitForSeconds(delayTime);
        }
    }

    private IEnumerator Phase4(int delayTime, Transform dominationPointLocation)
    {
        var dominationPoint = Instantiate(m_DominationPoint, 
                                          dominationPointLocation.position, 
                                          dominationPointLocation.rotation);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_Enemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_BigEnemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_RangedEnemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in dominationPointLocation)
        {
            Instantiate(m_Boss, spawnPoint.position, spawnPoint.rotation);  
        }

        while (m_Phase == 4)
        {    
            int time = dominationPoint.GetComponent<DominationPoint>().GetSeconds();
            
            if (time >= 20)
            {
                Destroy(dominationPoint);
                m_Phase = 5;
                m_PhaseCompleted = true;
                break;
            }

            yield return new WaitForSeconds(delayTime);
        }
    }

    private IEnumerator Phase5(int delayTime)
    {
        var dominationPoint = Instantiate(m_DominationPoint, 
                                          m_FinalDominationPointLocation.position, 
                                          m_FinalDominationPointLocation.rotation);


        foreach (Transform spawnPoint in m_FinalDominationPointLocation)
        {
            Instantiate(m_Enemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in m_FinalDominationPointLocation)
        {
            Instantiate(m_BigEnemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in m_FinalDominationPointLocation)
        {
            Instantiate(m_RangedEnemy, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in m_FinalDominationPointLocation)
        {
            Instantiate(m_Boss, spawnPoint.position, spawnPoint.rotation);  
        }

        yield return new WaitForSeconds(delayTime);

        foreach (Transform spawnPoint in m_FinalDominationPointLocation)
        {
            Instantiate(m_RangedBoss, spawnPoint.position, spawnPoint.rotation);  
        }

        while (m_Phase == 5)
        {    
            int time = dominationPoint.GetComponent<DominationPoint>().GetSeconds();
            
            if (time >= 20)
            {
                Destroy(dominationPoint);
                m_Phase = 6;
                m_PhaseCompleted = true;
                break;
            } 

            yield return new WaitForSeconds(delayTime);
        }
    }

    private void Win() 
    {
        Time.timeScale = 0f;
        Cursor.lockState = CursorLockMode.Confined;
        m_YouWinUI.SetActive(true);
    }
}
