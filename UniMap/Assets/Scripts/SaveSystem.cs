using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveSystem : MonoBehaviour
{
    public float mouseSensitivity = 5f;

    private void Awake()
    {
        string path = Application.persistentDataPath + "/playerstat.fun";
       
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, 
                                               FileMode.Open,       
                                               FileAccess.ReadWrite, 
                                               FileShare.None);

           mouseSensitivity = (float) formatter.Deserialize(stream);
        }
    }

    public float LoadMouseSensitivity()
    {   
        return  mouseSensitivity;
    }

    public void SaveMouseSensitivity()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerstat.fun";
        FileStream stream = new FileStream(path, 
                                           FileMode.Create,
                                           FileAccess.ReadWrite, 
                                           FileShare.None);

        formatter.Serialize(stream, mouseSensitivity);
        stream.Close();
    }



}
