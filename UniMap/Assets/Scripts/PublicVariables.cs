using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PublicVariables
{
    public static string BULLET_TAG = "bullet";
    public static float RELOAD_TIME = 3.09f;
    public static int TOTAL_BULLETS = 32;

    public static float PLAYER_BASE_SPEED = 5f;

    public static float FIRE_RATE = 0.5f;
}
