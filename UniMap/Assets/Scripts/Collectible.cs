using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    [SerializeField] private float m_ResetTime = 20f;

    private Transform m_Player;
    private PlayerHealth m_PlayerHealth;
    private LayerMask m_PlayerMask;
    private MeshRenderer m_MeshRenderer;
    private bool m_display = true;

    private void Start()
    {
        m_PlayerMask = LayerMask.GetMask("Player");
        m_Player = GameObject.Find("Player").transform;
        m_PlayerHealth = m_Player.GetComponent<PlayerHealth>();
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player") && m_display)
        {
            m_display = false;
            m_MeshRenderer.enabled = false;
            m_PlayerHealth.Heal(50);
            StartCoroutine(ResetObject());
        }
    }

    private IEnumerator ResetObject()
    {
        yield return new WaitForSeconds(m_ResetTime);
        m_display = true;
        m_MeshRenderer.enabled = true;
    }


}
