using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationObject : MonoBehaviour
{
    [SerializeField] private bool m_RotateY = false;
    [SerializeField] private float m_YRange = 2f;

    private Vector3 m_StartPosition;
    private float m_RotationSpeed = 100f;
    private Vector3 m_RotationVector;

    void Start () 
    {
        m_StartPosition = transform.position;
        if (!m_RotateY)
            m_RotationVector = new Vector3(m_RotationSpeed, m_RotationSpeed, m_RotationSpeed);
        else
            m_RotationVector = new Vector3(0, m_RotationSpeed, 0);
    }
    
    void Update()
    {
        transform.Rotate(m_RotationVector * Time.deltaTime);
        transform.position = m_StartPosition + new Vector3(0f, m_YRange * Mathf.Sin(Time.time), 0f);
    }
}
