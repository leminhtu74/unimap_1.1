using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSkybox : MonoBehaviour
{
    [SerializeField] private Material[] m_SkyboxArray;

    void Start()
    {
        int index = Random.Range(0, m_SkyboxArray.Length);  
        RenderSettings.skybox = m_SkyboxArray[index];
    }
}
