using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLook : MonoBehaviour
{
    [SerializeField] private Slider m_Slider;

    public Camera m_Camera;
    private float m_XRotation = 0f;
    
    private SaveSystem m_Save;
    private float m_Sensitivity;

    private void Start() 
    {
        m_Save = GetComponent<SaveSystem>();
        m_Sensitivity = m_Save.mouseSensitivity;
        m_Slider.value = m_Sensitivity;
    }

    public void ChangeMouseSensitivity(float newSensitivity)
    {
        m_Save.mouseSensitivity = newSensitivity;
        m_Sensitivity = m_Save.mouseSensitivity;
        m_Save.SaveMouseSensitivity();
    }

    public void ProcessLook(Vector2 input)
    {
        float mouseX = input.x;
        float mouseY = input.y;

        m_XRotation -= (mouseY * Time.deltaTime) * m_Sensitivity;
        m_XRotation = Mathf.Clamp(m_XRotation, -80f, 80f);

        m_Camera.transform.localRotation = Quaternion.Euler(m_XRotation, 0, 0);

        transform.Rotate(Vector3.up * (mouseX * Time.deltaTime) * m_Sensitivity);
    }
}
